from flask import Flask, jsonify
from logger.logger_base import log
from models.computer_model import ComputerModel
from services.computer_service import ComputerService
from routes.computer_routes import ComputerRoutes

app = Flask(__name__)

#creamos la instancia del modelo
computer_model = ComputerModel()

#creamos la instancia del servicio 
computer_service = ComputerService(computer_model)

#creamos la instancia de las rutas
computer_routes = ComputerRoutes(computer_service)

#registramos el blueprint de las rutas
app.register_blueprint(computer_routes)

try:
    #nos conectamos a la base de datos
    computer_model.connect_to_database()
except Exception as e:
    log.critical(e)

if __name__ == '__main__':
    app.run()
