from marshmallow import Schema, fields, ValidationError

class ComputerSchema(Schema):
    name = fields.String(required=True)
    type = fields.String(required=True)
    marca = fields.String(required=True)
    modelo = fields.String(required=True)
    serial_number = fields.Str(required=True)
    state = fields.String(required=True)
    register_date = fields.DateTime(required=True)
    location = fields.String(required=True)
    technical_details = fields.Dict(required=True)