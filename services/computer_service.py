from logger.logger_base import log
from flask import jsonify
from models.computer_model import ComputerModel

class ComputerService:
    def __init__(self, db_conn):
        self.db_conn = db_conn

    def get_computers(self):
        try:
            computers = list(self.db_conn.db.computers.find())
            return computers
        
        except Exception as e:
            error_msg = f'Error fetching computers from the database {e}'
            raise Exception(error_msg)
        
        
    def get_computer_by_id(self, id: int):
        try:
            computer = self.db_conn.db.computers.find_one({'_id': id})
            return computer
        
        except Exception as e:
            error_msg = f'Error fetching computer from the database {e}'
            raise Exception(error_msg)
        
    def add_computer(self, computer):
        try:
            return self.db_conn.db.computers.insert_one(computer)
        except Exception as e:
            error_msg = f'Error adding a new computer {e}'
            raise Exception(error_msg)
        
    def update_computer(self, computer, id: int):
        try:
            filter = {'_id': id}
            data = {"$set": computer}
            return self.db_conn.db.computers.update_one(filter, data)
        except Exception as e:
            error_msg = f'Error updating the computer, with id {id} {e}'
            raise Exception(error_msg)
        
    def delete_computer(self, id:int):
        try:
            filter = {'_id': id}
            deleted_computer = self.db_conn.db.computers.delete_one(filter)
            if deleted_computer.deleted_count  > 0: 
                return True
            else:
                return False

        except Exception as e:
            error_msg = f'Error deleting the computer, with id {id} {e}'
            raise Exception(error_msg)
        
if __name__ == '__main__':
    db = ComputerModel()
    db.connect_to_database()

    computer = {
        "name": "Samsung Galaxy Book",
        "type": "Laptop",
        "marca": "Samsung",
        "modelo": "GalaxyBook2024",
        "serial_number": "SG2024-654321",
        "state": "active",
        "register_date": "2024-04-10T10:30:00Z",
        "location": "Remote Office",
        "technical_details": {
            "processor": "Intel Core i5",
            "ram": "8GB",
            "storage": "256GB SSD",
            "operating_system": "Windows 11"
        }
    }

    ComputerService(db).add_computer(computer)
