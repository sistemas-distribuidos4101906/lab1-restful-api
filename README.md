# Lab RESTful API
Author: Vazquez Sanchez Erick Alejandro

To create mongodb container use the following command:
```
docker run -d -p 27017:27017 \
--name mongodb -v ~/mongodb_data:/data/db \
-e MONGO_INITDB_ROOT_USERNAME=erick \
-e MONGO_INITDB_ROOT_PASSWORD=hola123. \
mongo:7.0.8
```

To install dependencies create a virtualenv and activate. After that install the dependecies inside `requirements.txt` file using the following command:
```
pip3 install -r requirements.txt
```
