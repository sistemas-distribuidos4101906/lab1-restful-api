from flask import Blueprint, jsonify, request
from logger.logger_base import log
from services.computer_service import ComputerService
from schemas.computer_schema import ComputerSchema
from marshmallow import ValidationError

class ComputerRoutes(Blueprint):
    def __init__(self, computer_service: ComputerService):
        super().__init__('computer', __name__)
        self.computer_service = computer_service
        self.register_routes()

    def register_routes(self):
        self.route('/api/computers', methods=['GET'])(self.get_books)
        self.route('/api/computers/<int:id>', methods=['GET'])(self.get_computer_by_id)
        self.route('/api/computers', methods=['POST'])(self.add_computer)
        self.route('/api/computers/<int:id>', methods=['PUT'])(self.update_computer)
        self.route('/api/computers/<int:id>', methods=['DELETE'])(self.delete_computer)


    def get_books(self):
        try:
            computers = self.computer_service.get_computers()
            return jsonify(computers)
        
        except Exception as error_msg:
            log.critical(error_msg)
            return jsonify({'error': str(error_msg)}), 500
    
    def get_computer_by_id(self, id):
        try:
            computer = self.computer_service.get_computer_by_id(id)

            if computer == None:
                return jsonify({'error': f'Cannot found computer for id: {id}'})
            
            return jsonify({'computer': computer}), 201
        
        except Exception as error_msg:
            log.critical(error_msg)

            return jsonify({'error': str(error_msg)}), 500
    
    def add_computer(self):
        req_body = request.get_json()

        if not req_body:
            return jsonify({'error': 'Invalid data'})
        
        try:
            data = ComputerSchema().load(req_body)
            new_computer = self.computer_service.add_computer(data)
            
            if new_computer == None:
                msg = "Cannot add the computer in the datebase"
                log.critical(msg)
                return jsonify('error:', msg)
            
            return jsonify({'message': 'Computer added succesfully'}), 201

        except ValidationError as e:
            # 400 -> bad request
            return jsonify({'error': e.messages}), 400
        
        except Exception as error_msg: 
            log.critical(error_msg)

            return jsonify({'error': str(error_msg)}), 500


    def update_computer(self, id: int):
        req_body = request.get_json()

        if not req_body:
            return jsonify({'error': 'Invalid data'})
        
        try:
            data = ComputerSchema().load(req_body)
            updated_computer = self.computer_service.update_computer(data, id)
            
            if updated_computer == None:
                msg = f"Cannot update the computer with id {id} in the datebase"
                log.critical(msg)
                return jsonify('error:', msg)
            
            return jsonify({'message': 'Computer updated succesfully'}), 201

        except ValidationError as e:
            # 400 -> bad request
            return jsonify({'error': e.messages}), 400
        
        except Exception as error_msg: 
            log.critical(error_msg)

            return jsonify({'error': str(error_msg)}), 500

    def delete_computer(self, id: int):
        try:
            done = self.computer_service.delete_computer(id)

            if done:
                return jsonify({'message': 'Computer deleted successfully'})
            else:
                return jsonify({'error': f'Cannot delete computer with id {id}'}), 404
        
        except Exception as error_msg:
            log.critical(error_msg)
            return jsonify({'error': str(error_msg)}), 500