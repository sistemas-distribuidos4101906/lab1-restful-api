import os
from pymongo import MongoClient
from logger.logger_base import log

class ComputerModel:
    def __init__(self):

        self.client = None
        self.db = None

    def connect_to_database(self):
        mongodb_user = os.environ.get('MONGODB_USER')
        mongodb_pass = os.environ.get('MONGODB_PASS')
        mongodb_host = os.environ.get('MONGODB_HOST')

        missing_vars = [var for var, val in locals().items() if val is None]
        if missing_vars:
            missing_vars_str = ', '.join(missing_vars)
            log.critical(f'Variables required not found: {missing_vars_str}')
            raise ValueError(f'Set enviroment variables: {missing_vars_str}')
        
        try:
            self.client = MongoClient(
                host=mongodb_host,
                port=27017,
                username=mongodb_user,
                password=mongodb_pass,
                authSource='admin',
                authMechanism='SCRAM-SHA-256',
                serverSelectionTimeoutMS=5000
            )

            self.db = self.client['computers_db']
            if self.db.list_collection_names():
                log.info('Connected to MongoDB succesfully')
        except Exception as e:
            log.critical(f'Failed to connect to MongoDB: {e}')
            raise
            
    def close_connection(self):
        if self.client:
            self.client.close()
    
if __name__ == '__main__':
    computer_conection = ComputerModel()
    try:
        computer_conection.connect_to_database()
        log.info('Connected to database')
    except Exception as e:
        log.critical(f'Error: {e}')
    finally:
        computer_conection.close_connection()
        log.info('Connection close')