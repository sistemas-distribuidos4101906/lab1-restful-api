import logging as log

log.basicConfig(
    level=log.DEBUG,
    format='%(asctime)s: %(levelname)s [%(filename)s: %(lineno)s] %(message)s',
    datefmt='%I:%M:%S %p',
    handlers=[
        log.FileHandler('lab1_restful_api.log'),
        log.StreamHandler()
    ]
)

if __name__ == '__main__':
    log.debug('Test debug')
    log.info('Test info')
    log.warning('Test warning')
    log.error('Test error')
    log.critical('Test critical')